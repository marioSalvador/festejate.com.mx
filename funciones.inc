<?php
session_start();
include("config.php");
$_GET['a']= (isset($_GET['a']))?_d($_GET['a']):"Inicio";
function _d($var)
{
	$dato =	htmlentities($var,ENT_QUOTES,'UTF-8');
	$dato = stripslashes($dato);
	return trim($dato);
}

function obtenIP()
{
	if(isset($_SERVER)) {
		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
			$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		} else if(isSet($_SERVER["HTTP_CLIENT_IP"])) {
			$realip = $_SERVER["HTTP_CLIENT_IP"];
		} else {
			$realip = $_SERVER["REMOTE_ADDR"];
		}
	} else {
		if( getenv( 'HTTP_X_FORWARDED_FOR' ) ) {
			$realip = getenv( 'HTTP_X_FORWARDED_FOR' );
		} else if( getenv( 'HTTP_CLIENT_IP' ) ) {
			$realip = getenv( 'HTTP_CLIENT_IP' );
		} else {
			$realip = getenv( 'REMOTE_ADDR' );
		}
	}
	return $realip;
}

function esEmail($email = null)
{
	$car = "/^([a-z0-9+_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,6}\$/i";
	if (strpos($email, '@') !== false && strpos($email, '.') !== false)
	{
		if (preg_match($car, $email)) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}	
	
function codificaMail($email=null)
{
	$mailCodificado = "";
    for ($i=0; $i < strlen($email); $i++)
	{
        if(rand(0,1) == 0)
		{
            $mailCodificado .= "&#" . (ord($email[$i])) . ";";
        }
		else
		{
            $mailCodificado .= "&#X" . dechex(ord($email[$i])) . ";";
        }
    }
	return $mailCodificado;
}

function limpiaEmail($email)
{
	$limpio = preg_replace('/[^a-z0-9+_.@-]/i', '', $email);
	return strtolower(_d($limpio));
}

function enviaContacto($nombre,$mail,$telefono,$comentario)	
{
	$asunto = "Nuevo comentario - FESTEJATE.COM.MX" ;
    $cuerpo = "<center><div style='width:500px; border:#810081 solid 2px; background:#fff; padding:15px; text-align:left;'>
    		   <p>Nuevo comentario desde el sitio web: festejate.com.mx</p>
    		   <p><b>Nombre: </b> $nombre</p>
    		   <p><b>Email: </b> $mail</p>
    		   <p><b>Tel&eacute;fono: </b> $telefono</p>
    		   <p><b>Comentario: </b> $comentario</p> 
			   </div></center>";
    $headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\n";
    $headers .= "From: 'Festejate' <info@festejate.com.mx>";
    mail("info@festejate.com.mx",$asunto,$cuerpo,$headers);
    mail("israel.sanchez.padilla@gmail.com",$asunto,$cuerpo,$headers);
    return true;
}

function dameTitulo()
{
	$var = "30% de Descuento en ";
	$var2 = "Renta de mobiliario Lounge :: ";
	switch($_GET['a'])
	{
		default:
		case "Inicio": echo $var."Renta de Lounge, Salas Lounge, Mobiliario Lounge, Lounge D.F."; break;
		case "Lounge": echo $var."Renta de Salas Lounge D.F. Decoraci&oacute;n y Ambientaci&oacute;n Lounge"; break;
			case "Arena": echo $var."Mobiliario Lounge :: Arena "; break;
				case "Violeta": echo $var.$var2."tem&aacute;tica Arena &gt; Violeta"; break;				
				case "Desertica": echo $var.$var2."tem&aacute;tica Arena &gt; Des&eacute;rtica"; break;				
				case "Tulum": echo $var.$var2."tem&aacute;tica Arena &gt; Tul&uacute;m"; break;				
				case "TeaTime": echo $var.$var2."tem&aacute;tica Arena &gt; Tea Time"; break;				
			case "Blanc": echo $var."Renta de Mobiliario Lounge :: Blanc "; break;	
				case "Mojito": echo $var.$var2."tem&aacute;tica Blanc &gt; Mojito"; break;
				case "Frida": echo $var.$var2."tem&aacute;tica Blanc &gt; Frida"; break;
				case "Joy": echo $var.$var2."tem&aacute;tica Blanc &gt; Joy"; break;
				case "Equinox": echo $var.$var2."tem&aacute;tica Blanc &gt; Equinox"; break;				
			case "Grey": echo $var."Renta de mobiliario Lounge :: Grey "; break;
				case "Gin": echo $var.$var2."tem&aacute;tica Grey &gt; Gin"; break;				
				case "Dry": echo $var.$var2."tem&aacute;tica Grey &gt; Dry"; break;				
				case "Stone": echo $var.$var2."tem&aacute;tica Grey &gt; Stone"; break;																		
			case "Chocolate": echo $var."Renta de mobiliario Lounge :: Chocolate "; break;
				case "Cocoa": echo $var.$var2."tem&aacute;tica Chocolate &gt; Cocoa"; break;
				case "Toscana": echo $var.$var2."tem&aacute;tica Chocolate &gt; Toscana"; break;
				case "Classic": echo $var.$var2."tem&aacute;tica Chocolate &gt; Classic"; break;
			case "Onyx": echo $var."Renta de mobiliario Lounge :: Onyx "; break;
				case "Beat": echo $var.$var2."tem&aacute;tica Onyx &gt; Beat"; break;
				case "Night": echo $var.$var2."tem&aacute;tica Onyx &gt; Night"; break;
				case "Shine": echo $var.$var2."tem&aacute;tica Onyx &gt; Shine"; break;										
				case "LondonBar": echo $var.$var2."tem&aacute;tica Onyx &gt; London Bar"; break;
				case "LondonBarAndWine": echo $var.$var2."tem&aacute;tica Onyx &gt; London Bar and Wine"; break;										
				case "BlackVelvet": echo $var.$var2."tem&aacute;tica Onyx &gt; Black Velvet"; break;										
				case "Tinto": echo $var.$var2."tem&aacute;tica Onyx &gt; Tinto "; break;					
			case "Fresh": echo $var."Renta de mobiliario Lounge :: Fresh"; break;
				case "Beach": echo $var.$var2."tem&aacute;tica Fresh &gt; Beach"; break;
				case "Garden": echo $var.$var2."tem&aacute;tica Fresh &gt; Garden";  break;
				case "Spring": echo $var.$var2."tem&aacute;tica Fresh &gt; Spring";  break;
				case "Florence": echo $var.$var2."tem&aacute;tica Fresh &gt; Florence";  break;															
			case "Nosotros": echo $var."Decoraci&oacute:n Lounge, Fiestas Lounge, Lounge VIP "; break;
			case "Contacto": echo $var."Salas Lounge, Mobiliario Lounge, Lounge D.F., Espacios Lounge "; break;
	}
}

/*function generaServicios()
{
	$servicios = array("Mobiliario Lounge VIP", "Ambientaci&oacute;n &amp; Decoraci&oacute;n","Dise&ntilde;o floral","Banquetes","Barra de bebidas","Mesa de postres","Audio &amp; DJ",
	"Iluminaci&oacute;n","Performance","Escenograf&iacute;a","Producci&oacute;n de Video");
	$tamanos = array(70,65,60,55,50,45,40,35,30,25,20);
	for($i=0;$i<21;$i++)
	{
		echo "<span class='a ";
		echo ($i%2==0)?"gris'":"'";
		echo "style='font-size:".$tamanos[rand(0,count($tamanos)-1)]."px'>".$servicios[rand(0,count($servicios)-1)]."</span> ";
	}
}*/

?>