<div id="body">
	<div style="width: 100%; margin: 0 auto">
        <img style="max-width: 100%;"src="imagenes/promo-festejate1.jpg">
    </div>
    <div class="flex">
		<?php include("lineas.php"); ?>
	    <div id="linea">
			<div id="titulo">
				<h3 class='m'>Stone</h3>
			</div>
	        <div id="imgL">
				<div id="ant"><a id="prev" href="#"><img src="imagenes/ant.png" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" /></a></div>
	            <div id="s2" class="lins">
	            	<?php
					for($i=1;$i<4;$i++)
	            		printf("<img src='imagenes/stone/%s.jpg' alt='%s' title='%s' />",$i,alt(),title());
					?>
	        	</div>
				<div id="sig"><a id="next" href="#"><img src="imagenes/sig.png" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" /></a></div>
			</div>
	    </div>
    </div>
</div>