<?php if(ANALITYCS) {?>
<script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="js/cycle.js"></script> 
<script language="javascript" type="text/javascript" src="js/color.js"></script>
<script language="javascript" type="text/javascript" src="js/jshowoff.js"></script>
<script language="javascript" type="text/javascript" src="js/timers.js"></script>
<script language="javascript" type="text/javascript" src="js/galleryview.js"></script>
<script src="js/remodal.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myModal').modal('toggle');
    });
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-3915289-24']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script src="js/main.js"></script>
<?php } ?>
