<?php
if($_POST)
{	
	include_once("funciones.inc");
	$nombre = urldecode(str_replace("n=","",_d($_POST["n"])));
	$mail = urldecode(str_replace("e=","",_d($_POST["e"])));
	$telefono = urldecode(str_replace("t=","",_d($_POST["t"])));
	$comentario = urldecode(str_replace("c=","",_d($_POST["c"])));
	
	if(empty($nombre))
	{
		exit("<h6 class='alert'><img src='imagenes/icoAlert.gif' width='14' /> Ingresa tu nombre.</h6>");
	}
	else if(empty($mail))
	{
		exit("<h6 class='alert'><img src='imagenes/icoAlert.gif' width='14' /> Ingresa tu email.</h6>");
	}
	else if(!esEmail($mail))
	{
		exit("<h6 class='alert'><img src='imagenes/icoAlert.gif' width='14' /> Email incorrecto.</h6>");
	}
	else if(empty($telefono))
	{
		exit("<h6 class='alert'><img src='imagenes/icoAlert.gif' width='14' /> Ingresa tu n&uacute;mero telef&oacute;nico.</h6>");
	}
	else if(empty($comentario))
	{
		exit("<h6 class='alert'><img src='imagenes/icoAlert.gif' width='14' /> Ingresa tu comentario.</h6>");
	}
	else
	{
		if($_SESSION['a'])
		{
			if(enviaContacto($nombre,$mail,$telefono,$comentario))
			{
				$_SESSION['a'] = false;
                
                $archivo = "visitas-mail.txt";
                $fp = fopen($archivo,"a");
                fwrite($fp, $mail."\n");
                fclose($fp);
				
                echo "<h6 class='correcto c'><img src='imagenes/icoCorrecto.png' width='12' /> Gracias. Hemos recibido tus datos.</h6>";
			}
			else 
			{
				echo "<h6 class='error'><img src='imagenes/icoError.gif' width='12' /> Error. Intenta nuevamente.</h6>";
			}
		}
		else
		{
			exit("<h6 class='alert'><img src='imagenes/icoAlert.gif' width='14' /> Gracias. Ya hemos recibido tus datos.</h6>");
		}
	}
}
else
{
	exit("<h6><img src='imagenes/icoError.gif' width='12' /> <b>ERROR. No hay datos.</b></h6>");
}
?>
