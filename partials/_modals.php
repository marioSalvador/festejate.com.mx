<div class="remodal-bg"></div>
<div class="remodal" data-remodal-id="modal1">
  <button data-remodal-action="close" class="remodal-close text-white"></button>
  <h1 class="text-center">Gin</h1>
  <div class="row center-xs middle-xs">
    <div class="col-md-4">
      <img src="imagenes/gin/1.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/gin/2.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/gin/3.png" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
  </div>
</div>

<div class="remodal" data-remodal-id="modal2">
  <button data-remodal-action="close" class="remodal-close text-white"></button>
  <h1 class="text-center">Aqua</h1>
  <div class="row center-xs middle-xs">
    <h2 class="text-white">Galería no disponible</h2>
  </div>
</div>

<div class="remodal" data-remodal-id="modal3">
  <button data-remodal-action="close" class="remodal-close text-white"></button>
  <h1 class="text-center">Stone</h1>
  <div class="row center-xs middle-xs">
    <div class="col-md-4">
      <img src="imagenes/stone/1.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/stone/2.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/stone/3.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
  </div>
</div>

<div class="remodal" data-remodal-id="modal4">
  <button data-remodal-action="close" class="remodal-close text-white"></button>
  <h1 class="text-center">Dry</h1>
  <div class="row center-xs middle-xs">
    <div class="col-md-4">
      <img src="imagenes/dry/1.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/dry/2.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/dry/3.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
  </div>
</div>

<div class="remodal" data-remodal-id="modal5">
  <button data-remodal-action="close" class="remodal-close text-white"></button>
  <h1 class="text-center">London Bar</h1>
  <div class="row center-xs middle-xs">
    <div class="col-md-4">
      <img src="imagenes/londonbar/1.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/londonbar/2.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/londonbar/3.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>    
  </div>
</div>

<div class="remodal" data-remodal-id="modal6">
  <button data-remodal-action="close" class="remodal-close text-white"></button>
  <h1 class="text-center">Frida</h1>
  <div class="row center-xs middle-xs">
    <div class="col-md-4">
      <img src="imagenes/frida/1.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/frida/2.png" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/frida/3.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>    
  </div>  
</div>
<div class="remodal" data-remodal-id="modal7">
  <button data-remodal-action="close" class="remodal-close text-white"></button>
  <h1 class="text-center">Night</h1>
  <div class="row center-xs middle-xs">
    <div class="col-md-4">
      <img src="imagenes/night/1.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/night/2.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/night/3.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>    
  </div>
</div>

<div class="remodal" data-remodal-id="modal8">
  <button data-remodal-action="close" class="remodal-close text-white"></button>
  <h1 class="text-center">Beach</h1>
  <div class="row center-xs middle-xs">
    <div class="col-md-4">
      <img src="imagenes/beach/1.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/beach/2.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/beach/3.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>    
  </div>  
  <button data-remodal-action="close" class="remodal-close"></button>
</div>

<div class="remodal" data-remodal-id="modal9">
  <button data-remodal-action="close" class="remodal-close text-white"></button>
  <h1 class="text-center">Black Velvet</h1>
  <div class="row center-xs middle-xs">
    <div class="col-md-4">
      <img src="imagenes/blackvelvet/1.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/blackvelvet/2.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>
    <div class="col-md-4">
      <img src="imagenes/blackvelvet/3.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" class="img-responsive">
    </div>    
  </div>
</div>