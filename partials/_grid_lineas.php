<div class="row center-xs middle-xs top-space">
	<?php
		$limit = 9;
		$lineas = array(
			'Gin',
			'Aqua',
			'Stone',
			'Dry',
			'London bar',
			'Frida',
			'Night',
			'Beach',
			'Black Velvet'
		);
		for($i = 1; $i <= $limit; $i++) {
			echo '
				<div class="col-md-4 top-space">
						<img src="imagenes/lounge/'.$i.'.jpg" alt="'.alt().'" title="'.title().'" class="img-responsive event-box" id="img_'.$i.'">
						<p class="text-center box">'.$lineas[$i - 1].'</p>
				</div>
			';
		}
	?>
</div>
<!-- Modals -->
<?php include('_modals.php'); ?>