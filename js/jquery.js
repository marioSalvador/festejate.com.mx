$(document).ready(function(){
					 
	$("#nav li a.a").hover(function() {
        $(this).animate({ color: "#F91CF9" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	$(".link").hover(function() {
        $(this).animate({ color: "#FFFFFF" }, 500);
        },function() {
	    $(this).animate({ color: "#F91CF9" }, 500);
        });
	$("#servicios p span.a").hover(function() {
        $(this).animate({ color: "#F91CF9" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//verde
	$("#lineas li a.v").hover(function() {
        $(this).animate({ color: "#A5C534" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//dorado
	$("#lineas li a.d").hover(function() {
        $(this).animate({ color: "#D6952A" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//turquesa
	$("#lineas li a.tu").hover(function() {
        $(this).animate({ color: "#5C9398" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//morado
	$("#lineas li a.m").hover(function() {
        $(this).animate({ color: "#810081" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//rojo
	$("#lineas li a.r").hover(function() {
        $(this).animate({ color: "#FF0000" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//azul
	$("#lineas li a.az").hover(function() {
        $(this).animate({ color:"#394C6A" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//azul quemado
	$("#lineas li a.azq").hover(function() {
        $(this).animate({ color:"#2E4A6B" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//amarillo
	$("#lineas li a.am").hover(function() {
        $(this).animate({ color:"#EACD0C" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//arena
	$("#lineas li a.ar").hover(function() {
        $(this).animate({ color:"#ECD1AA" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//gris
	$("#lineas li a.g").hover(function() {
        $(this).animate({ color:"#ECD1AA" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//rosa
	$("#lineas li a.ro").hover(function() {
        $(this).animate({ color:"#FF00FF" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	//vino
	$("#lineas li a.vi").hover(function() {
        $(this).animate({ color:"#622A33" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
		//cobre
	$("#lineas li a.ca").hover(function() {
        $(this).animate({ color:"#AA5500" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
		//violeta
	$("#lineas li a.vo").hover(function() {
        $(this).animate({ color:"#664A9F" }, 500);
        },function() {
	    $(this).animate({ color: "#FFFFFF" }, 500);
        });
	
	
	/*servicios*/
	$("#ml").hover(function() {
        $("#mlinfo").fadeIn('normal');
        },function() {
	    $("#mlinfo").fadeOut('fast');
        });
	
	//silla tifany
	$("#st").hover(function() {
        $("#stinfo").fadeIn('normal');
        },function() {
	    $("#stinfo").fadeOut('fast');
        });
	
	//ambientacion y decoracion
	$("#ad").hover(function() {
        $("#adinfo").fadeIn('normal');
        },function() {
	    $("#adinfo").fadeOut('fast');
        });
	
	//dise�o floral
	$("#df").hover(function() {
        $("#dfinfo").fadeIn('normal');
        },function() {
	    $("#dfinfo").fadeOut('fast');
        });
	
	//banquetes
	$("#ba").hover(function() {
        $("#bainfo").fadeIn('normal');
        },function() {
	    $("#bainfo").fadeOut('fast');
        });
	
	//barra bebidas
	$("#bb").hover(function() {
        $("#bbinfo").fadeIn('normal');
        },function() {
	    $("#bbinfo").fadeOut('fast');
        });
	
	//dj
	$("#dj").hover(function() {
        $("#djinfo").fadeIn('normal');
        },function() {
	    $("#djinfo").fadeOut('fast');
        });
	
	//performance
	$("#pe").hover(function() {
        $("#peinfo").fadeIn('normal');
        },function() {
	    $("#peinfo").fadeOut('fast');
        });
	
	//escenografia
	$("#es").hover(function() {
        $("#esinfo").fadeIn('normal');
        },function() {
	    $("#esinfo").fadeOut('fast');
        });
	
	//fotografia
	$("#fo").hover(function() {
        $("#foinfo").fadeIn('normal');
        },function() {
	    $("#foinfo").fadeOut('fast');
        });
	
	
	
	
	
	$('#s2').cycle({
    fx:     'fade',
    speed:  'fast',
    timeout: 0,
    next:   '#next',
    prev:   '#prev'
    });

	$("#bn").cycle({ 
	    fx:    "fade", 
    	speed:  3000,
		pause: 1
	 });
	
	$("#nav ul ").css({display: "none"}); 
	$("#nav li").hover(function(){
		$(this).find('ul:first').css({visibility: "visible",display: "none"}).show('slow');
		},function(){
		$(this).find('ul:first').css({visibility: "hidden"}).hide('slow');
	});
	
	$("#lineasNav li").click(function(){
		var image = $(this).attr("id");
		$('#linea').hide();
		$('#linea').fadeIn('slow');	
		$('#linea').html('<a href="'+image+'"><img src="imagenes/' + image + '.jpg"/></a>');
	});
	
	$(document).ready(function(){ $('#features').jshowoff(); });
	
	$('.btnEnviar').click(function(event)
	{
		$("#resp").html("<h6 class='load'><img src='imagenes/icoLoad.gif' width='12' /> Enviando informaci&oacute;n.</h6>")
		$("#resp").load("enviaContacto.php",{n:$("#n").serialize(),e:$("#e").serialize(),t:$("#t").serialize(),c:$("#c").serialize()})
   	});
	
	$(".input").focus(function(){$(this).css({'background-color' : '#FFF'});});
    $(".input").blur(function(){$(this).css({'background-color' : '#E7E7E7'});});
	
	$(document).ready(function(){ $('#myGallery').galleryView(); });
	
});