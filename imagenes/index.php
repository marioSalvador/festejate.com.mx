<?php include("funciones.inc"); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php dameTitulo(); ?>Festejate</title>
<link rel="stylesheet" type="text/css" href="css/main.css" media="all" />
</head>
<body>
<div>
	<div class="contenedor">
		<?php 
		include("head.php"); 
		switch($_GET['a'])
		{
			default:
			case "Inicio": include("principal.php"); break;
			case "Lounge": include("loung.php"); break;
				case "Arena": include("aren_a.php"); break;
					case "Violeta": include("violet_a.php"); break;				
					case "Desertica": include("desertic_a.php"); break;				
					case "Tulum": include("tulu_m.php"); break;				
					case "TeaTime": include("teatim_e.php"); break;				
				case "Blanc": include("blan_c.php"); break;	
					case "Mojito": include("mojit_o.php"); break;
					case "Frida": include("frid_a.php"); break;
					case "Joy": include("jo_y.php"); break;
					case "Equinox": include("equino_x.php"); break;				
				case "Grey": include("gre_y.php"); break;
					case "Gin": include("gi_n.php"); break;				
					case "Dry": include("dr_y.php"); break;				
					case "Stone": include("ston_e.php"); break;																		
				case "Chocolate": include("chocolat_e.php"); break;
					case "Cocoa": include("coco_a.php"); break;
					case "Toscana": include("toscan_a.php"); break;
					case "Classic": include("classi_c.php"); break;
				case "Onyx": include("ony_x.php"); break;
					case "Beat": include("bea_t.php"); break;
					case "Night": include("nigh_t.php"); break;
					case "Shine": include("shin_e.php"); break;										
					case "LondonBar": include("londonba_r.php"); break;
					case "LondonBarAndWine": include("londonwin_e.php"); break;										
					case "BlackVelvet": include("blackvelve_t.php"); break;										
					case "Tinto": include("tint_o.php"); break;					
				case "Fresh": include("fres_h.php"); break;
					case "Beach": include("beac_h.php"); break;
					case "Garden": include("garde_n.php"); break;
					case "Spring": include("sprin_g.php"); break;
					case "Florence": include("florenc_e.php"); break;
			case "Galeria": include("gale.php");break;
			case "Nosotros": include("nos.php"); break;
			case "Servicios": include("serv.php"); break;
			case "Paquetes": include("paq.php"); break;
			case "Flores": include("flors.php"); break;
			case "Contacto": $_SESSION['a'] = true; include("contact.php"); break;
		}
	   	include("pie.php"); ?>
    </div>
</div>
<?php include("analitycs.php"); ?>
</body>
</html>
