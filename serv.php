<div id="body"><br /><br />
<h3 class="rosa text-middle top">El especialista Fest&eacute;jate.</h3>
<p>Solicita una cita con nosotros y descubre todos nuestros paquetes y servicios que har&aacute;n de tu evento un momento maravilloso.</p>
<p>En Fest&eacute;jate Lounge &amp; Catering, descubrir&aacute;s una amplia gama de servicios, los cuales nuestros expertos se encargaran de personalizar cada uno de ellos para que tu evento sea totalmente exclusivo.</p>
<!-- 	<div class="media">
        <ul id="servicios">
            <li><a href="#" class="link" id="ml">Mobiliario Lounge VIP</a></li>
            <li><a href="#" class="link" id="st">Silla Tiffany</a></li>
            <li><a href="#" class="link" id="ad">Ambientaci&oacute;n &amp; Decoraci&oacute;n</a></li>
            <li><a href="#" class="link" id="df">Dise&ntilde;o floral</a></li>
            <li><a href="#" class="link" id="ba">Banquetes</a></li>
            <li><a href="#" class="link" id="bb">Barra de bebidas</a></li>
            <li><a href="#" class="link" id="dj">Audio DJ &amp; iluminaci&oacute;n</a></li>
            <li><a href="#" class="link" id="pe">Performance</a></li>
            <li><a href="#" class="link" id="es">Escenograf&iacute;a</a></li>
            <li><a href="#" class="link" id="fo">Fotograf&iacute;a</a></li>
        </ul>
    </div> -->
<!--     <div class="media">
    	<div class="serv" id="mlinfo">
        	<h3 class="rosa">Mobiliario Lounge</h3>
            <img src="imagenes/servML.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
        <div class="serv oc" id="stinfo">
        	<h3 class="rosa">Silla Tiffany</h3>
            <img src="imagenes/servST.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
        <div class="serv oc" id="adinfo">
        	<h3 class="rosa">Ambientaci&oacute;n &amp; Decoraci&oacute;n</h3>
            <img src="imagenes/servAD.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
        <div class="serv oc" id="dfinfo">
        	<h3 class="rosa">Dise&ntilde;o floral</h3>
            <img src="imagenes/servDF.jpg"alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
         <div class="serv oc" id="bainfo">
        	<h3 class="rosa">Banquetes</h3>
            <img src="imagenes/servBA.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
         <div class="serv oc" id="bbinfo">
        	<h3 class="rosa">Barra de bebidas</h3>
            <img src="imagenes/servBB.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
        <div class="serv oc" id="djinfo">
        	<h3 class="rosa">Audio DJ &amp; iluminaci&oacute;n </h3>
            <img src="imagenes/servDJ.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
        <div class="serv oc" id="peinfo">
        	<h3 class="rosa">Performance</h3>
            <img src="imagenes/servPE.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
        <div class="serv oc" id="esinfo">
        	<h3 class="rosa">Escenograf&iacute;a</h3>
            <img src="imagenes/servES.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
        <div class="serv oc" id="foinfo">
        	<h3 class="rosa">Fotograf&iacute;a</h3>
            <img src="imagenes/servFO.jpg" alt="<?php echo alt(); ?>" title="<?php echo title(); ?>" />
        </div>
    </div> -->
    <h3 class="text-middle">Servicios</h3>
    <div class="row">
        <div class="col-md-6">
            <ul id="servicios" class="padding-middle">
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Renta de salas Lounge</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Renta de mesas &amp; sillas</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Renta de periqueras</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Servicio de banquetes</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Servicio de barra de bebidas</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Servicio de carrito de shots</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Servicio de mezcladores</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Mesa de dulces - Candy bar</a></li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul id="servicios" class="padding-middle">
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Coordinación de bodas</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Decoración de eventos</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Diseño floral</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Dj's</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Audio &amp; iluminaci&oacute;n</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Performance</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Fotos Booth</a></li>
                <li><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> <a href="#" class="link" id="ml">Fiestas tema</a></li>
            </ul>            
        </div>
    </div>
    <p>Nuestro equipo de expertos te brindar&aacute; asesor&iacute;a personalizada para seleccionar los servicios adecuados que har&aacute;n de tu evento algo &uacute;nico. <u>Conoce la amplia gama de servicios que ponemos a tus  ordenes.</u></p>
</div>
