<div id="body">
	<div id="nos"> 
		<div class="media">&nbsp;</div>
    	<div class="media">
	    <h1>Festejate</h1>
	    <p>Somos una empresa vanguardista, conformada por un equipo de profesionales, apasionados por el dise&ntilde;o,
	    la creaci&oacute;n y la coordinaci&oacute;n integral de eventos. Cuidamos cada detalle, para lograr siempre un
	    concepto &uacute;nico y exclusivo para cada uno de nuestros clientes.</p>
	    <p>Nos distinguen el compromiso, la puntualidad, la excelente actitud y nuestras innovadoras propuestas.</p>
	    <p>Te invitamos a conocer <a href="Servicios" class="link">nuestros servicios</a> que har&aacute;n de tu evento una experiencia inolvidable.</p>
    	</div>
      </div>
</div>